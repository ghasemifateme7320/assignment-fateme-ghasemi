import { CreateUserDto } from './dto/create-user.dto';
import { Controller, Get, Post, Delete, Param, Body,Injectable } from '@nestjs/common';
import { UsersService } from './users.service';
import { RabbitMQService } from '../services/rabbitmq.service';
import { EmailService } from '../services/email.service';
@Controller('api/users')
export class UsersController {
  constructor(private readonly usersService: UsersService,
              private readonly rabbitMQService: RabbitMQService,
              private readonly emailService: EmailService
              ) {}
  @Post()
  async create(
    @Body() createUserDto: CreateUserDto) {
    const user =  this.usersService.create(createUserDto);
    await this.emailService.sendEmail(createUserDto.email, 'User Created', `Welcome, ${createUserDto.name}!`);

    //It can be fire and forget and no need to has await
    this.rabbitMQService.publishEvent({ type: 'userCreated', data: user }, 'user-events');
    return user
  }

  @Get()
  async findAll() {
    return  this.usersService.findAll();
  }

  @Get(':userId')
  async findOne(@Param('userId') userId: string) {
    return this.usersService.findOneById(userId);
  }

  @Delete(':userId')
  async deleteOne(@Param('userId') userId: string) {
    return this.usersService.deleteOne(userId);
  }
}
