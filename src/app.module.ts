import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { UsersController } from './users/users.controller';
import { UsersService } from './users/users.service';
import { RabbitMQService } from './services/rabbitmq.service';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [UsersModule,
    MongooseModule.forRoot('mongodb://root:example@mongo:27017/'),
    UsersModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
