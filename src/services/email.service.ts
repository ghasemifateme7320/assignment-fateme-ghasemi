import { Injectable } from '@nestjs/common';

@Injectable()
export class EmailService {
  async sendEmail(to: string, subject: string, body: string): Promise<void> {
    // Simulate sending email by logging a message
    console.log(`Sent email to ${to} with subject: ${subject} and body: ${body}`);
  }
}
