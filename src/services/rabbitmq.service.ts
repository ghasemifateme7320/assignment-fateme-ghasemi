// users/rabbitmq.service.ts

import { Injectable } from '@nestjs/common';
import * as amqp from 'amqplib';

@Injectable()
export class RabbitMQService {
  private readonly rabbitMQUrl = 'amqp://localhost'; // Update with your RabbitMQ URL

  async publishEvent(event: any, queueName: string): Promise<void> {
    const connection = await amqp.connect(this.rabbitMQUrl);
    const channel = await connection.createChannel();

    await channel.assertQueue(queueName, { durable: true });

    const message = JSON.stringify(event);
    channel.sendToQueue(queueName, Buffer.from(message), { persistent: true });

    setTimeout(() => {
      connection.close();
    }, 500);
  }
}
